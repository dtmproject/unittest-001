
/**
 * @file unittest.cc
 * @author Uwe Koecher (UK)
 * @date 2018-06-12, UK
 *
 * @brief unit test for TrilinosWrappers::SparseMatrix add / copy_from problem
 * 
 * NOTE: in release mode the Assert of the Trilinos ierr is not thrown,
 *       compare the (at least) output files M_0 (incorrect) and Muu_0 (correct)
 *       with e.g. diff Madd_0.gpl Muu_0.gpl , respectively, Mcopy_0.gpl
 */

////////////////////////////////////////////////////////////////////////////////
// NOTE: by undefining the following, the problem does not occur
//       since all rows are stored in a contiguous order
#define DoFRenumbering_component_wise
////////////////////////////////////////////////////////////////////////////////

/*  Copyright (C) 2012-2018 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

// DEAL.II includes
#include <deal.II/base/exceptions.h>
#include <deal.II/base/logstream.h>
#include <deal.II/base/utilities.h>

#include <deal.II/base/mpi.h>
#include <deal.II/base/multithread_info.h>
#include <deal.II/base/table.h>
#include <deal.II/base/numbers.h>

#include <deal.II/base/conditional_ostream.h>
#include <deal.II/base/index_set.h>

#include <deal.II/distributed/tria.h>
#include <deal.II/distributed/grid_refinement.h>

#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_renumbering.h>
#include <deal.II/dofs/dof_tools.h>

#include <deal.II/fe/fe_system.h>
#include <deal.II/fe/fe_dgq.h>

#include <deal.II/grid/tria.h>
#include <deal.II/grid/grid_generator.h>

#include <deal.II/lac/constraint_matrix.h>
#include <deal.II/lac/trilinos_sparsity_pattern.h>
#include <deal.II/lac/block_sparsity_pattern.h>
#include <deal.II/lac/vector_operation.h>

#include <deal.II/lac/trilinos_sparse_matrix.h>
using ParallelSparseMatrix = dealii::TrilinosWrappers::SparseMatrix;

// C++ includes
#include <iostream>
#include <fstream>
#include <memory>
#include <vector>

////////////////////////////////////////////////////////////////////////////////
#define REFINE_GLOBAL 2
#define FE_P 1

#define MPIX_THREADS 1

////////////////////////////////////////////////////////////////////////////////
template<int dim>
class UnitTest
{
public:
	UnitTest () :
		mpi_comm(MPI_COMM_WORLD),
		pcout(
			std::cout,
			!dealii::Utilities::MPI::this_mpi_process(mpi_comm)
		)
	{
		const unsigned int NumProc{
			dealii::Utilities::MPI::n_mpi_processes(mpi_comm)
		};
		
		AssertThrow(
			NumProc > 1,
			dealii::ExcMessage(
				"you need to run this program with at least 2 MPI processes"
			)
		);
	};
	
	virtual ~UnitTest() {
		if (dof.use_count()) {
			dof->clear();
		}
	}
	
	void run () {
		make_grid();
		setup_fe();
		distribute();
		
		make_partitioning();
		make_constraints();
		
		make_sparsity_pattern();
		
		reinit_system_matrix();
		
		assemble_system();
		
		// FIXME: TrilinosWrappers::SparseMatrix::add( factor, other_matrix )
		//        fails for reordered dofs, such that the rows are not in a
		//        contiguous order
		// 
		// NOTE: look into the implementation for further notes.
		failing_add_function();
		
		failing_copy_from_function();
	};
	
private:
	void make_grid ();
	void setup_fe ();
	void distribute ();
	
	void make_partitioning ();
	void make_constraints ();
	
	void make_sparsity_pattern ();
	
	void reinit_system_matrix ();
	
	void assemble_system ();
	
	void failing_add_function ();
	void failing_copy_from_function ();
	
	MPI_Comm mpi_comm;
	dealii::ConditionalOStream pcout;
	
	std::shared_ptr< dealii::parallel::distributed::Triangulation<dim> > tria;
	std::shared_ptr< dealii::DoFHandler<dim> > dof;
	std::shared_ptr< dealii::FESystem<dim> > fe;
	
	std::shared_ptr< dealii::IndexSet > locally_owned_dofs;
	std::shared_ptr< dealii::IndexSet > locally_relevant_dofs;
	
	std::shared_ptr< std::vector< dealii::IndexSet > > partitioning_locally_owned_dofs;
	std::shared_ptr< std::vector< dealii::IndexSet > > partitioning_locally_relevant_dofs;
	
	std::shared_ptr< dealii::ConstraintMatrix > constraints;
	
	std::shared_ptr< dealii::TrilinosWrappers::SparsityPattern > sp_M;
	
	std::shared_ptr< ParallelSparseMatrix > M;    ///< L^2 mass matrix

};


////////////////////////////////////////////////////////////////////////////////
template<int dim>
void UnitTest<dim>::
make_grid ()
{
	tria = std::make_shared< dealii::parallel::distributed::Triangulation<dim> >(
		mpi_comm,
		typename dealii::Triangulation<dim>::MeshSmoothing(
			dealii::Triangulation<dim>::smoothing_on_refinement |
			dealii::Triangulation<dim>::smoothing_on_coarsening
		)
	);
	
	dof = std::make_shared< dealii::DoFHandler<dim> >(*tria);
	
	dealii::GridGenerator::hyper_cube(
		*tria,
		0.0, 1.0
	);
	
	tria->refine_global(REFINE_GLOBAL);
	
	pcout
		<< "Number of active cells: "
		<< tria->n_active_cells()
		<< std::endl;
}


template<int dim>
void UnitTest<dim>::
setup_fe ()
{
	fe = std::make_shared< dealii::FESystem<dim> > (
		// mechanics FE (component 0 ... dim-1)
		dealii::FESystem<dim> (
			dealii::FE_DGQ<dim>(FE_P), dim
		), 1
	);
	
	pcout
		<< "ewave_dG: created " << fe->get_name()
		<< std::endl;
}


template<int dim>
void UnitTest<dim>::
distribute ()
{
	AssertThrow(fe.use_count(), dealii::ExcNotInitialized());
	AssertThrow(dof.use_count(), dealii::ExcNotInitialized());
	dof->distribute_dofs(*fe);
	
	pcout
		<< "ewave_dG: distributed dofs"
		<< std::endl;
	
#ifdef DoFRenumbering_component_wise
	dealii::DoFRenumbering::component_wise(*dof);
	
	pcout
		<< "ewave_dG: dealii::DoFRenumbering::component_wise(*dof);"
		<< std::endl;
#endif
}


template<int dim>
void UnitTest<dim>::
make_partitioning ()
{
	AssertThrow(dof.use_count(), dealii::ExcNotInitialized());
	
	// get all dofs componentwise
	std::vector< dealii::types::global_dof_index > dofs_per_component(
		dof->get_fe_collection().n_components(), 0
	);
	dealii::DoFTools::count_dofs_per_component(*dof, dofs_per_component, true);
	
	// set specific values of dof counts
	dealii::types::global_dof_index N_u = 0;
	for (unsigned int d{0}; d < dim; ++d) {
		N_u += dofs_per_component[0*dim+d]; // Number of DoF of the displacement field
	}
	
	// set specific global dof offset values
	dealii::types::global_dof_index N_u_offset = 0;
	
	pcout << "ewave_dG: distribute_dofs overview: " << std::endl;
	pcout << "\tN_u = " << N_u << std::endl;
	pcout << "\tN_u_offset = " << N_u_offset << std::endl;
	pcout << "\tn_dofs = " << dof->n_dofs() << std::endl;
	
	// create distributed-parallel dof partitionings 
	locally_owned_dofs = std::make_shared< dealii::IndexSet >();
	*locally_owned_dofs = dof->locally_owned_dofs();
	
	locally_relevant_dofs = std::make_shared< dealii::IndexSet >();
	dealii::DoFTools::extract_locally_relevant_dofs(*dof, *locally_relevant_dofs);
	
	partitioning_locally_owned_dofs =
		std::make_shared< std::vector< dealii::IndexSet > >();
	partitioning_locally_owned_dofs->push_back(
		locally_owned_dofs->get_view(N_u_offset, N_u_offset+N_u)
	);
	
	partitioning_locally_relevant_dofs =
		std::make_shared< std::vector< dealii::IndexSet > >();
	partitioning_locally_relevant_dofs->push_back(
		locally_relevant_dofs->get_view(N_u_offset, N_u_offset+N_u)
	);
	
	////////////////////////////////////////////////////////////////////////////
	// check if each MPI process locally owns dofs
	// otherwise the problem might not occur
	
	unsigned int local_is_empty{
		static_cast<unsigned int> (locally_owned_dofs->is_empty() ? 1 : 0)
	};
	
	unsigned int global_is_empty;
	
	MPI_Allreduce(
		&local_is_empty,
		&global_is_empty,
		1,
		MPI_UNSIGNED,
		MPI_SUM,
		mpi_comm
	);
	
	pcout
		<< "global is empty = " << global_is_empty
		<< std::endl;
	
	AssertThrow(
		global_is_empty==0,
		dealii::ExcMessage(
			"At least one of your MPI processes does not have dofs. \n"
			"    Refine the grid or Reduce the number of MPI processes."
		)
	);
}


template<int dim>
void UnitTest<dim>::
make_constraints ()
{
	AssertThrow(locally_relevant_dofs.use_count(), dealii::ExcNotInitialized());
	
	constraints =
		std::make_shared< dealii::ConstraintMatrix > ();
	
	constraints->clear();
	constraints->reinit(
		*locally_relevant_dofs
	);
	
	constraints->close();
	
	pcout
		<< "ewave_dG: constraints object"
		<< std::endl;
}


template<int dim>
void UnitTest<dim>::
make_sparsity_pattern ()
{
	////////////////////////////////////////////////////////////////////////////
	// Operator M basis function couplings
	dealii::Table<2,dealii::DoFTools::Coupling> coupling_block_M(
		dim, dim
	);
	{
		// loop 0: init with non-coupling
		for (unsigned int i{0}; i < dim; ++i)
		for (unsigned int j{0}; j < dim; ++j) {
			coupling_block_M[i][j] = dealii::DoFTools::none;
		}
		// loop 1a: init with coupling for chi-chi (matrix M_uu)
		for (unsigned int i{0}; i < dim; ++i)
		for (unsigned int j{0}; j < dim; ++j) {
			coupling_block_M[i][j] = dealii::DoFTools::always;
		}
	}
	
	// biot: "M" block matrix
	auto sp_block_M =
		std::make_shared< dealii::TrilinosWrappers::BlockSparsityPattern > ();
	
	AssertThrow(
		partitioning_locally_owned_dofs.use_count(),
		dealii::ExcNotInitialized()
	);
	
	sp_block_M->reinit(
		*partitioning_locally_owned_dofs,
		mpi_comm
	);
	
	AssertThrow(
		constraints.use_count(),
		dealii::ExcNotInitialized()
	);
	
	dealii::DoFTools::make_sparsity_pattern(
		*dof,
		coupling_block_M,
		*sp_block_M,
		*constraints,
		false, // keep constrained dofs?
		dealii::Utilities::MPI::this_mpi_process(mpi_comm)
	);
	
	sp_block_M->compress();
	
	sp_M =
		std::make_shared< dealii::TrilinosWrappers::SparsityPattern > ();
	
	sp_M->copy_from(sp_block_M->block(0,0));
	
	// debug output of the distributed SparsityPattern
	// through all processes
	// can be plotted with plotdrop sp_M* (gnuplot)
// #ifdef DEBUG
	const unsigned int MyPID{
		dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)
	};
	
	{
		std::ostringstream filename;
		filename << "sp_M_" << MyPID << ".gpl";
		std::ofstream outM(filename.str().c_str(), std::ios_base::out);
		
		sp_M->print_gnuplot(outM);
	}
// #endif
	
	pcout
		<< "ewave_dG: created SparsityPattern"
		<< std::endl;
}


template<int dim>
void UnitTest<dim>::
reinit_system_matrix ()
{
	AssertThrow(
		sp_M.use_count(),
		dealii::ExcNotInitialized()
	);
	
	M = std::make_shared< ParallelSparseMatrix > ();
	M->reinit(*sp_M);
	
	pcout
		<< "ewave_dG: reinit mass matrix M "
		<< std::endl;
}


template<int dim>
void UnitTest<dim>::
assemble_system ()
{
	AssertThrow(
		M.use_count(),
		dealii::ExcNotInitialized()
	);
	
	// NOTE: we only set dummy entries into M instead of assembling the matrix
	//       this is fine here for this unit test
	
	*M = 0;
	
	AssertThrow(
		locally_owned_dofs.use_count(),
		dealii::ExcNotInitialized()
	);
	
	for (unsigned int i{0}; i < locally_owned_dofs->n_elements(); ++i) {
		pcout
			<< "i = " << i
			<< ", corresponding global i = "
			<< locally_owned_dofs->nth_index_in_set(i)
			<< std::endl;
		
		
		M->set(
			locally_owned_dofs->nth_index_in_set(i),
			locally_owned_dofs->nth_index_in_set(i),
			dealii::numbers::PI
		);
	}
	
	M->compress(
		dealii::VectorOperation::insert
	);
	
	// debug output of the distributed dummy mass matrix M
	// through all processes
	// can be plotted with plotdrop Muu_* (gnuplot)
	// or read into other number cruncer programs for debugging issues
// #ifdef DEBUG
	{
		const unsigned int MyPID{
			dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)
		};
		
		std::ostringstream filename;
		filename << "Muu_" << MyPID << ".gpl";
		std::ofstream out(filename.str().c_str(), std::ios_base::out);
		
		M->print(out);
		out.close();
	}
// #endif
	
	pcout
		<< "ewave_dG: assemble M (dummy entries on diagonal)... (done)"
		<< std::endl;
}


template<int dim>
void UnitTest<dim>::
failing_add_function ()
{
	AssertThrow(
		M.use_count(),
		dealii::ExcNotInitialized()
	);
	
	auto new_M = std::make_shared< ParallelSparseMatrix > ();
	new_M->reinit(*sp_M);
	
	*new_M = 0;
	
	////////////////////////////////////////////////////////////////////////////
	// FIXME: Trilinos ierr thrown is:
	// An error occurred in line <1762> of file </apps/candi/deal.II-toolchain-unpack/deal.II-v9.0.0/source/lac/trilinos_sparse_matrix.cc> in function
	//     void dealii::TrilinosWrappers::SparseMatrix::add(dealii::TrilinosScalar, const dealii::TrilinosWrappers::SparseMatrix&)
	// The violated condition was: 
	//     ierr == 0
	// Additional information: 
	//     An error with error number -1 occurred while calling a Trilinos function
	//
	// NOTE: BUGFIX: ::add(dealii::TrilinosScalar, const dealii::TrilinosWrappers::SparseMatrix&)
	//		assumes wrongly that all rows are contiguous, which is here not the case
	//
	// NOTE: FIXME: the same happens for ::copy_from(  ) for the same issue
	
	new_M->add(1.0, *M);
	
	// debug output of the distributed new matrix new_M
	// through all processes
	// can be plotted with plotdrop Madd_* (gnuplot)
	// or read into other number cruncer programs for debugging issues
// #ifdef DEBUG
	{
		const unsigned int MyPID{
			dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)
		};
		
		std::ostringstream filename;
		filename << "Madd_" << MyPID << ".gpl";
		std::ofstream out(filename.str().c_str(), std::ios_base::out);
		
		new_M->print(out);
		out.close();
	}
// #endif
	
	pcout
		<< "failing_add_function (done) "
		<< std::endl;
}


template<int dim>
void UnitTest<dim>::
failing_copy_from_function ()
{
	AssertThrow(
		M.use_count(),
		dealii::ExcNotInitialized()
	);
	
	auto new_M = std::make_shared< ParallelSparseMatrix > ();
	
	////////////////////////////////////////////////////////////////////////////
	// FIXME: Trilinos ierr thrown is:
	// An error occurred in line <409> of file < source/lac/trilinos_sparse_matrix.cc> in function
    //   void dealii::TrilinosWrappers::SparseMatrix::copy_from(const dealii::TrilinosWrappers::SparseMatrix&)
	// The violated condition was: 
	// 		ierr == 0
	// Additional information: 
    //		An error with error number -1 occurred while calling a Trilinos function
	//
	// NOTE: BUGFIX: ::copy_from(SparseMatrix&)
	//		assumes wrongly that all rows are contiguous, which is here not the case
	
	new_M->reinit(*sp_M); // avoid deep copy trunc within copy_from( )
	new_M->copy_from(*M);
	
	// debug output of the distributed new matrix new_M
	// through all processes
	// can be plotted with plotdrop M_* (gnuplot)
	// or read into other number cruncer programs for debugging issues
// #ifdef DEBUG
	{
		const unsigned int MyPID{
			dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)
		};
		
		std::ostringstream filename;
		filename << "Mcopy_" << MyPID << ".gpl";
		std::ofstream out(filename.str().c_str(), std::ios_base::out);
		
		new_M->print(out);
		out.close();
	}
// #endif
	
	pcout
		<< "failing_add_function (done) "
		<< std::endl;
}


////////////////////////////////////////////////////////////////////////////////
int main(int argc, char *argv[]) {
	// Init MPI (or MPI+X)
	dealii::Utilities::MPI::MPI_InitFinalize mpi(argc, argv, MPIX_THREADS);
	
	try {
// 		dealii::deallog.depth_console(0);
		
		auto problem = std::make_shared<UnitTest<2>> ();
		
		problem->run();
		
	}
	catch (std::exception &exc) {
		if (!dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)) {
			std::cerr	<< std::endl
						<< "****************************************"
						<< "****************************************"
						<< std::endl << std::endl
						<< "An EXCEPTION occured: Please READ the following output CAREFULLY!"
						<< std::endl;
			
			std::cerr	<< exc.what() << std::endl;
			
			std::cerr	<< std::endl
						<< "APPLICATION TERMINATED unexpectedly due to an exception."
						<< std::endl << std::endl
						<< "****************************************"
						<< "****************************************"
						<< std::endl << std::endl;
		}
		
		return 1;
	}
	catch (...) {
		if (!dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)) {
			std::cerr	<< std::endl
						<< "****************************************"
						<< "****************************************"
						<< std::endl << std::endl
						<< "An UNKNOWN EXCEPTION occured!"
						<< std::endl;
			
			std::cerr	<< std::endl
						<< "----------------------------------------"
						<< "----------------------------------------"
						<< std::endl << std::endl
						<< "Further information:" << std::endl
						<< "\tThe main() function catched an exception"
						<< std::endl
						<< "\twhich is not inherited from std::exception."
						<< std::endl
						<< "\tYou have probably called 'throw' somewhere,"
						<< std::endl
						<< "\tif you do not have done this, please contact the authors!"
						<< std::endl << std::endl
						<< "----------------------------------------"
						<< "----------------------------------------"
						<< std::endl;
			
			std::cerr	<< std::endl
						<< "APPLICATION TERMINATED unexpectedly due to an exception."
						<< std::endl << std::endl
						<< "****************************************"
						<< "****************************************"
						<< std::endl << std::endl;
		}
		
		return 1;
	}
	
	return 0;
}
